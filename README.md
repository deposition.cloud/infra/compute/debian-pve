# PVE on Debian

refs: 
- [Install Proxmox VE on Debian 12 Bookworm](https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_12_Bookworm)
- [📹 Deploying Machines with MaaS and Packer - Metal as a Service + Hashicorp Packer Tutorial](https://www.youtube.com/watch?v=lEqD3mRcqSo)


```bash
# Add Proxmox VE repository and import its GPG key
echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bookworm pve-no-subscription" | sudo tee /etc/apt/sources.list.d/pve-install-repo.list
sudo wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
sudo dpkg --configure -a # should return nothing: this generally indicates that all packages have been configured properly, and there are no errors pending resolution

# Update system and upgrade packages
sudo apt update && sudo apt full-upgrade --yes

# Check for and correct any broken dependencies
sudo apt -f install
# Remove no longer required packages 
sudo apt autoremove --yes

# Automatically get the IP address of the specified network interface (enp2s0)
sudo apt install jq --yes
IP=$(ip -j a | jq -r '.[] | select(.ifname=="enp2s0") | .addr_info[] | select(.family=="inet") | .local' | tr -d '\n')

# Update the hosts template to use the acquired IP address instead of the placeholder
sudo sed -i "s|127.0.1.1 {{fqdn}} {{hostname}}|$IP {{fqdn}} {{hostname}}|" /etc/cloud/templates/hosts.debian.tmpl

# Initialize hostname changes with cloud-init
sudo cloud-init clean --logs
sudo cloud-init init
cat /etc/hosts # to confirm the changes

# Install Proxmox VE kernel
sudo apt install proxmox-default-kernel --yes

# Reboot to ensure the system uses the Proxmox kernel
sudo systemctl reboot

# Remove default Debian kernel to avoid conflicts
sudo apt remove linux-image-amd64 'linux-image-6.1*' --yes
sudo update-grub

# Set postfix to install silently without interactive configuration
echo "postfix postfix/mailname string christina.science" | sudo debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | sudo debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt install postfix --yes

# After reboot, install remaining Proxmox and necessary packages
sudo apt install proxmox-ve open-iscsi chrony --yes

# Check for and correct any broken dependencies
sudo apt -f install

# Might need a reboot first
sudo systemctl reboot
sudo apt -f install

# Setup root password
sudo passwd root

# Reboot for fun
sudo systemctl reboot